package com.dv.minimaltasksapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MinimalTasksApplication : Application()